package com.linkstec.base.entity;

import lombok.Data;

@Data
public class Account {

    //id
    private int id;
    //账户号
    private String account;
    //名字
    private String name;
    //手机号码
    private String mobile;
    //邮箱
    private String email;
}
