package com.linkstec.base.entity;

import lombok.Data;

@Data
public class User {

   private String id;
   //企业微信id
   private String userId;
   //账户号
   private String account;
   //名称
   private String name;
   //性别
   private String gender;
   //手机号码
   private String mobile;
   //邮箱
   private String email;


}
