package com.linkstec.base.controller;

import com.linkstec.base.util.DataResultUtil;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@RequestMapping(value = "/test/{url}", method = RequestMethod.GET)
	public DataResultUtil postUser(@PathVariable("url") String url) {
		return DataResultUtil.getSuccessResult("成功, url=" + url);
	}

}
