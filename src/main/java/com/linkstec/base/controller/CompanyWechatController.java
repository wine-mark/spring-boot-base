package com.linkstec.base.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.linkstec.base.menus.TicketTypeEnum;
import com.linkstec.base.service.WechatExternalContactService;
import com.linkstec.base.util.DataResultUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;

@RestController
@RequestMapping("/linkstec-qywechat")
public class CompanyWechatController {

	Logger logger = LoggerFactory.getLogger(CompanyWechatController.class);

	@Autowired
	private WechatExternalContactService externalContactService;

	/**
	 * 员工token的post接口
	 * @param url
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/post/user/{url}")
	public DataResultUtil postUser(@PathVariable("url") String url, @RequestBody JSONObject jsonObject) {
		try {
			return externalContactService.sendPostUser(jsonObject, url);
		} catch (Exception e) {
			logger.error("员工token的post接口错误! url={}", url);
			return DataResultUtil.getFailureResult("员工token的post接口错误!", url);
		}
	}

	/**
	 * 员工token的get接口
	 * @param url
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/get/user/{url}")
	public DataResultUtil getUser(@PathVariable("url") String url, @RequestBody JSONObject jsonObject) {
		try {
			return externalContactService.sendGetUser(jsonObject, url);
		} catch (Exception e) {
			logger.error("员工token的post接口错误! url={}", url);
			return DataResultUtil.getFailureResult("员工token的post接口错误!", url);
		}
	}

	/**
	 * 员工token的get接口 通过公司账号获取
	 * @param url
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/get/account/{url}")
	public DataResultUtil getAccount(@PathVariable("url") String url, @RequestBody JSONObject jsonObject) {
		try {
			return externalContactService.sendGetUserByAccount(jsonObject, url);
		} catch (Exception e) {
			logger.error("员工token的接get口错误! url={}", url);
			return DataResultUtil.getFailureResult("员工token的get接口错误!", url);
		}
	}







	/**
	 * 客户token的post接口
	 * @param url
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/post/customer/{url}")
	public DataResultUtil postCustomer(@PathVariable("url") String url, @RequestBody JSONObject jsonObject) {
		try {
			return externalContactService.sendPostCustomer(jsonObject, url);
		} catch (Exception e) {
			logger.error("员工token的post接口错误! url={}", url);
			return DataResultUtil.getFailureResult("员工token的post接口错误!", url);
		}
	}

	/**
	 * 客户token的get接口
	 * @param url
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/get/customer/{url}")
	public DataResultUtil getCustomer(@PathVariable("url") String url, @RequestBody JSONObject jsonObject) {
		try {
			return externalContactService.sendGetCustomer(jsonObject, url);
		} catch (Exception e) {
			logger.error("员工token的post接口错误! url={}", url);
			return DataResultUtil.getFailureResult("员工token的post接口错误!", url);
		}
	}

	/**
	 * 获取config参数
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/get/config/parameter")
	public DataResultUtil getConfigParameter(@RequestBody JSONObject jsonObject) {
		String url = jsonObject.getString("url");
		try {
			if(StringUtils.isEmpty(url)) {
				return DataResultUtil.getFailureResult("url不能为空",null);
			}
			url = URLDecoder.decode(url, "utf-8");
			return externalContactService.getAgentConfigParameter(url, TicketTypeEnum.CONFIG);
		} catch (Exception e) {
			e.printStackTrace();
			return DataResultUtil.getFailureResult("查询异常",null);
		}
	}

	/**
	 * 获取AgentConfig参数
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/get/agentConfig/parameter")
	public DataResultUtil getAgentConfigParameter(@RequestBody JSONObject jsonObject) {
		String url = jsonObject.getString("url");
		try {
			if(StringUtils.isEmpty(url)) {
				return DataResultUtil.getFailureResult("url不能为空",null);
			}
			url = URLDecoder.decode(url, "utf-8");
			return externalContactService.getAgentConfigParameter(url,TicketTypeEnum.AGENT_CONFIG);
		} catch (Exception e) {
			e.printStackTrace();
			return DataResultUtil.getFailureResult("查询异常",null);
		}
	}

}
