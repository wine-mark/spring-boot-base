package com.linkstec.base.constant;

public class SysConstant {

	public static String spot = "\\.";
	public static String slash = "\\/";

	//redis存储token key
	public static String base_redis_user_token_key = "base_redis_user_token";
	public static String base_redis_customer_token_key = "base_redis_customer_token";
	//redis存储AGENT_CONFIG
	public static String AGENT_CONFIG = "AGENT_CONFIG";
	//redis存储CONFIG
	public static String CONFIG = "CONFIG";
	//redis存储token time
	public static Long base_redis_time = 7200l;

	//微信redis存储
	//第三方平台ticket
	public static String ComponentVerifyTicket = "ComponentVerifyTicket";
	// 第三方平台token令牌
	public static String ComponentAccessToken = "component_access_token";
	//第三方平台预授权码
	public static String pre_auth_code = "pre_auth_code";

	//微信公众号jsapi_ticket
	public static String jsapi_ticket = "jsapi";
	//微信公众号access_token
	public static String access_token = "access_token";
}
