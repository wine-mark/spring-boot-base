package com.linkstec.base.service;

import com.alibaba.fastjson.JSONObject;
import com.linkstec.base.menus.TicketTypeEnum;
import com.linkstec.base.util.DataResultUtil;

public interface WechatExternalContactService {

	DataResultUtil sendGetUser(JSONObject jsonObject, String url);

	DataResultUtil sendGetCustomer(JSONObject jsonObject, String url);

	DataResultUtil sendPostUser(JSONObject jsonObject, String url);

	DataResultUtil sendPostCustomer(JSONObject jsonObject, String url);

	DataResultUtil getAgentConfigParameter(String url, TicketTypeEnum ticketTypeEnum);

	 DataResultUtil sendGetUserByAccount(JSONObject jsonObject, String url);
}
