package com.linkstec.base.service;

public interface WechatTokenManageService {

	// 获取员工token
	String getUserToken();

	// 获取客户token
	String getCustomerToken();

}
