package com.linkstec.base.service;

import com.alibaba.fastjson.JSONObject;
import com.linkstec.base.util.DataResultUtil;

public interface WechatConfigService {


    DataResultUtil getConfig(String url);

    String getAccessToken(String grant_type);
}
