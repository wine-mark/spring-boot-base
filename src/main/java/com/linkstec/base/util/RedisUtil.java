package com.linkstec.base.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName RedisUtil
 * @Description: Redis工具类
 * @Author linkage
 * @Date 2020/4/28  13:45
 * @Version V1.0
 **/
@Component
@Slf4j
public class RedisUtil {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除数据
     *
     * @param key
     */
    public void del(String key) {
        redisTemplate.delete(key);
    }

    /**
     * 获取数据
     *
     * @param key
     */
    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                redisTemplate.opsForValue().set(key, value);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 普通缓存放入,无过期时间
     *
     * @param key   键
     * @param value 值
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value) {
        return this.set(key, value, 0);
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */
    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(大于0)
     * @return
     */
    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * 加锁,setIfAbsent 表示key不存在的时候才可以设置成功，存在则设置失败
     *
     * @param key        键
     * @param value      对应的值
     * @param expireTime 过期时间，单位秒
     * @return true 加锁成功，false 加锁失败
     */
    public boolean getLock(String key, String value, int expireTime) {
        return redisTemplate.opsForValue().setIfAbsent(key, value, expireTime, TimeUnit.SECONDS);
    }

    /**
     * 释放锁，当key对应的value 与存储的值相同时，才可以释放锁
     *
     * @param key   键
     * @param value 对应的值
     * @return true 释放锁成功，false 释放锁失败
     */
    public boolean releaseLock(String key, String value) {
        // 1: 成功，0: 锁不存在，或锁已不归当前key 所有(value不一致）
        Long result = (Long) redisTemplate.execute(new DefaultRedisScript<Long>(
                "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end", Long.class), Arrays.asList(key), value);
        if (result != null && result > 0) {
            return true;
        } else {
            log.error("锁不存在，或锁已不归当前key 所有(value不一致） key:{} value:{}", key, value);
            return false;
        }
    }

}
