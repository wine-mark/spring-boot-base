package com.linkstec.base.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropUtil {

    //企业id
    @Value("${prop.qywechat.corpid}")
    public String prop_qywechat_corpid;
    //应用id
    @Value("${prop.qywechat.agentid}")
    public String prop_qywechat_agentid;
    //员工的corpsecret
    @Value("${prop.qywechat.user_corpsecret}")
    public String prop_qywechat_user_corpsecret;
    //客户的corpsecret
    @Value("${prop.qywechat.customer_corpsecret}")
    public String prop_qywechat_customer_corpsecret;

    //企业微信接口url
    @Value("${prop.qywechat.qyapi_url}")
    public String prop_qywechat_qyapi_url;
    //token获取接口
    @Value("${prop.qywechat.gettoken_url}")
    public String prop_qywechat_gettoken_url;
    //签名获取url
    @Value("${prop.qywechat.get_jsapi_ticket_url}")
    public String prop_qywechat_get_jsapi_ticket_url;

    //签名获取url
    @Value("${prop.qywechat.get_jsapi_agent_ticket_url}")
    public String prop_qywechat_get_jsapi_agent_ticket_url;


    //微信
    @Value("${prop.qywechat.get_jsapi_agent_ticket_url}")
    public String aesKey; //aeskey
    @Value("${prop.qywechat.get_jsapi_agent_ticket_url}")
    public String COMPONENT_TOKEN; //平台设定的token
    @Value("${prop.wechat.appid}")
    public String wechat_app_id;
    @Value("${prop.wechat.appsecret}")
    public String wechat_secret;
    //微信接口url
    @Value("${prop.wechat.api_url}")
    public String prop_wechat_api_url;
    //token获取接口
    @Value("${prop.wechat.token_url}")
    public String prop_wechat_token_url;
    @Value("${prop.wechat.ticket_url}")
    public  String prop_wechat_ticket_url;


}
