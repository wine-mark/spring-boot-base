package com.linkstec.base.mapper;

import com.linkstec.base.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;

@Mapper
@Resource
public interface UserInfoMapper {


    /**
     * 查找单个user通过公司账号
     * @param accountId
     * @return
     */
    User selectUserByAccountId(@Param("accountId") String accountId);

  /*  *//**
     * 查找所有user
     * @return
     *//*
    List<User> findUserList();

    *//**
     * 删除user
     * @param user
     * @return
     *//*
    int deleteUser(@Param("user")User user);*/



}
